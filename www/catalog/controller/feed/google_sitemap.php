<?php
class ControllerFeedGoogleSitemap extends Controller {
	public function index() {
		if ($this->config->get('google_sitemap_status')) {
			$output  = '<?xml version="1.0" encoding="UTF-8"?>';
			$output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
			//главная страница, куда ж без нее
				$output .= '<url>';
				$output .= '<loc>' . HTTPS_SERVER. '</loc>';
				$output .= '<changefreq>daily</changefreq>';
				$output .= '<priority>1.0</priority>';
				$output .= '</url>';

			$this->load->model('catalog/product');
			$this->load->model('catalog/category');

			$products = $this->model_catalog_product->getProducts();

			foreach ($products as $product) {
				//полный путь, включающий категории
				$main_category_id = $this->model_catalog_product->getProductMainCategory($product['product_id']);
				$categories_id = $this->model_catalog_category->getPath($main_category_id);
				$path='';
				$weight = 0.9;				
				if($categories_id){
					foreach($categories_id as $category_id){
						$path .=$category_id['path_id'].'_' ;
						$weight -=0.1;
					}
					if(strlen($path)>0){
						$path=substr($path,0,-1);
					}
				}

				$output .= '<url>';
				$output .= '<loc>' . $this->url->link('product/product','path='.$path.'&product_id='.$product['product_id']) . '</loc>';
				$output .= '<changefreq>weekly</changefreq>';
				$output .= '<priority>'.$weight.'</priority>';
				$output .= '</url>';
			}

			$output .= $this->getCategories(0);

			$this->load->model('catalog/manufacturer');

			$manufacturers = $this->model_catalog_manufacturer->getManufacturers();

			foreach ($manufacturers as $manufacturer) {
				$output .= '<url>';
				$output .= '<loc>' . $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id']) . '</loc>';
				$output .= '<changefreq>weekly</changefreq>';
				$output .= '<priority>0.9</priority>';
				$output .= '</url>';

				/*$products = $this->model_catalog_product->getProducts(array('filter_manufacturer_id' => $manufacturer['manufacturer_id']));

				foreach ($products as $product) {
					$output .= '<url>';
					$output .= '<loc>' . $this->url->link('product/product', 'manufacturer_id=' . $manufacturer['manufacturer_id'] . '&product_id=' . $product['product_id']) . '</loc>';
					$output .= '<changefreq>weekly</changefreq>';
					$output .= '<priority>1.0</priority>';
					$output .= '</url>';
				}*/
			}

			$this->load->model('catalog/information');

			$informations = $this->model_catalog_information->getInformations();

			foreach ($informations as $information) {
				$output .= '<url>';
				$output .= '<loc>' . $this->url->link('information/information', 'information_id=' . $information['information_id']) . '</loc>';
				$output .= '<changefreq>monthly</changefreq>';
				$output .= '<priority>0.5</priority>';
				$output .= '</url>';
			}
			//страница блог
			$output .= '<url>';
				$output .= '<loc>' . $this->url->link('pavblog/blogs').'</loc>' ;
				$output .= '<changefreq>weekly</changefreq>';
				$output .= '<priority>0.7</priority>';
				$output .= '</url>';
			//категорию верхнего уровня не выводим. Выводим его дочек с parent_id=1
			$this->load->model('pavblog/category');
			$parent_pavcategory_id=1;
			$pavcategories = $this->model_pavblog_category->getChildren($parent_pavcategory_id);
			foreach ($pavcategories as $pavcategory) {
				$output .= '<url>';
				$output .= '<loc>' . $this->url->link('pavblog/category', 'id=' . $pavcategory['category_id']) . '</loc>';
				$output .= '<changefreq>weekly</changefreq>';
				$output .= '<priority>0.6</priority>';
				$output .= '</url>';
			}
			//статьи Блога
			$this->load->model('pavblog/blog');
			$pavblogs = $this->model_pavblog_blog->getListBlogs(array());
			foreach ($pavblogs as $pavblog) {
				$output .= '<url>';
				$output .= '<loc>' . $this->url->link('pavblog/blog', 'id=' . $pavblog['blog_id']) . '</loc>';
				$output .= '<changefreq>monthly</changefreq>';
				$output .= '<priority>0.5</priority>';
				$output .= '</url>';
			}
			//страница контактов
			$output .= '<url>';
				$output .= '<loc>' . $this->url->link('information/contact') . '</loc>';
				$output .= '<changefreq>monthly</changefreq>';
				$output .= '<priority>0.5</priority>';
				$output .= '</url>';
			$output .= '</urlset>';

			$this->response->addHeader('Content-Type: application/xml');
			$this->response->setOutput($output);
		}
	}

	protected function getCategories($parent_id, $current_path = '') {
		$output = '';

		$results = $this->model_catalog_category->getCategories($parent_id);
		foreach ($results as $result) {
			if (!$current_path) {
				$new_path = $result['category_id'];
			} else {
				$new_path = $current_path . '_' . $result['category_id'];
			}
			$weight = 1;
			$path_arr = explode('_', $new_path);
			$level = count($path_arr);
			$weight -= ($level/10); 
			$output .= '<url>';
			$output .= '<loc>' . $this->url->link('product/category', 'path=' . $new_path) . '</loc>';
			$output .= '<changefreq>weekly</changefreq>';
			$output .= '<priority>'.$weight.'</priority>';
			$output .= '</url>';

			//так как в начале sitemap уже вывели товары с полным путем, содержащим основную категорию, здесь товары уже не нужны
			/*$products = $this->model_catalog_product->getProducts(array('filter_category_id' => $result['category_id']));

			foreach ($products as $product) {
				$output .= '<url>';
				$output .= '<loc>' . $this->url->link('product/product', 'path=' . $new_path . '&product_id=' . $product['product_id']) . '</loc>';
				$output .= '<changefreq>weekly</changefreq>';
				$output .= '<priority>1.0</priority>';
				$output .= '</url>';
			}*/

			$output .= $this->getCategories($result['category_id'], $new_path);
		}

		return $output;
	}
}
?>