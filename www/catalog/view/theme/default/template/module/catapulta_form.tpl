<!--..............Modal Window  for success message, when item was successfully added in cart -->  
<div class="modal fade" id="buy_1_click">
  <div class="modal-dialog">
    <div class="modal-content">
      	<div class="catapulta">
      		<h2 class="catapulta-title"><?php echo $heading_title; ?></h2>
    		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<?php if(isset($error_warning)) { ?>
			<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
			<?php if($stock_status) { ?>
				<?php if(isset($text_customer)) { ?>
					<?php echo $text_customer; ?>
				<?php } else { ?>
					<?php if($phone_text) { ?>
					<p class="contact-text"><?php echo $phone_text; ?></p>
					<?php } ?>
					<p class="contact-text"><?php echo $entry_contact; ?></p>
					<input type="text" name="catapulta_contact" value="" data-phone-mask="<?php echo $phone_mask; ?>" />
					<br class="contact_error" />
					<br />
					<div class="buttons">
						<div class="left"><a class= "catapulta-send button btn btn-shopping-cart" data-wait="<?php echo $text_wait; ?>"><?php echo $button_send; ?></a></div>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->