<?php

class ModelModuleIntegrationMS extends Model
{
    public function getIntegrationProductOptionValue($productOptionValueId)
    {
        $sql = sprintf("Select * from %sintegrationms_product_option WHERE product_option_value_id=%s",
            DB_PREFIX, $productOptionValueId);
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getIntegrationProduct($productId)
    {
        $sql = sprintf("Select * from %sintegrationms_product WHERE product_id=%s",
            DB_PREFIX, $productId);
        $query = $this->db->query($sql);

        return $query->row;
    }

}