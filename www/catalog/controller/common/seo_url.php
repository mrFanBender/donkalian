<?php
class ControllerCommonSeoUrl extends Controller {
	public function index() {
		// Add rewrite to url class
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}
		
		// Decode URL
		if (isset($this->request->get['_route_'])) {
			$parts = explode('/', $this->request->get['_route_']);
			
			foreach ($parts as $part) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "'");
				if ($query->num_rows) {
					$url = explode('=', $query->row['query']);
					
					if ($url[0] == 'product_id') {
						$this->request->get['product_id'] = $url[1];
					}
					if ($url[0] == 'contactpage') {
						$this->request->get['contactpage'] = $query->row['keyword'];
					}elseif($url[0] == 'blogpage') {
						$this->request->get['blogpage'] = $query->row['keyword'];
					}
					if ($url[0] == 'category_id') {
						if (!isset($this->request->get['path'])) {
							$this->request->get['path'] = $url[1];
						} else {
							$this->request->get['path'] .= '_' . $url[1];
						}
					}	
					
					if ($url[0] == 'manufacturer_id') {
						$this->request->get['manufacturer_id'] = $url[1];
					}
					
					if ($url[0] == 'information_id') {
						$this->request->get['information_id'] = $url[1];
					}
					if ($url[0] == 'pavblog/blog') {
						$this->request->get['pavblog/blog'] = $url[1];
						$this->request->get['id'] = $url[1];
					}
					if ($url[0] == 'pavblog/category') {
						$this->request->get['pavblog/category'] = $url[1];
						$this->request->get['id'] = $url[1];
					}
				}else {
					$this->request->get['route'] = 'error/not_found';	
				}
			}
			
			if (isset($this->request->get['product_id'])) {
				$this->request->get['route'] = 'product/product';
			} elseif (isset($this->request->get['path'])) {
				$this->request->get['route'] = 'product/category';
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$this->request->get['route'] = 'product/manufacturer/info';
			} elseif (isset($this->request->get['information_id'])) {
				$this->request->get['route'] = 'information/information';
			}elseif(isset($this->request->get['contactpage'])){
				$this->request->get['route'] = 'information/'.$this->request->get['contactpage'];
			}elseif(isset($this->request->get['blogpage'])){
				$this->request->get['route'] = 'pavblog/blogs';
			}elseif(isset($this->request->get['pavblog/category'])){
				$this->request->get['route'] = 'pavblog/category';
			}elseif(isset($this->request->get['pavblog/blog'])){
				$this->request->get['route'] = 'pavblog/blog';
			}
			
			if (isset($this->request->get['route'])) {
				return $this->forward($this->request->get['route']);
			}
		}
	}
	
	public function rewrite($link) {

		$url_info = parse_url(str_replace('&amp;', '&', $link));
	
		$url = ''; 
		
		$data = array();
		
		parse_str($url_info['query'], $data);
		
		foreach ($data as $key => $value) {
			if (isset($data['route'])) {
				/* новая seo-оптимизированная версия*/
				if(($data['route'] == 'product/product' && $key == 'product_id')){
					$url = ''; 
					$product_full_path_r = array();
					//получаем alias товара
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");
					if($query->num_rows){
						$product_full_path_r['product_alias'] = $query->row['keyword'];
						//получаем главную категорию
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "main_category WHERE `product_id` = " .(int)$value);
						if($query->num_rows){
							$product_main_category = $query->row['main_category_id'];
							//получаем список родительских категорий
							$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_path WHERE `category_id` =  ".$product_main_category." ORDER BY `level` DESC");
							if($query->num_rows){
								$categories_row = $query->rows;								
								foreach($categories_row as $category_row){
									$category_row['keywords'] = '';
									$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=". (int)$category_row['path_id'] . "'");
									if($query->num_rows){
										//$category_row['keywords'] = $query->row['keyword'];
										array_unshift($product_full_path_r, $query->row['keyword']);
									}
								}
							}

						}
						foreach($product_full_path_r as $path_part){
							$url .= '/' . $path_part;
						}
						unset($data[$key]);
					}

				}elseif ((($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id')) {
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");
				
					if ($query->num_rows) {
						$url .= '/' . $query->row['keyword'];
						
						unset($data[$key]);
					}					
				} elseif ($key == 'path') {
					$categories = explode('_', $value);
					
					foreach ($categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "'");
				
						if ($query->num_rows) {
							$url .= '/' . $query->row['keyword'];
						}							
					}
					
					unset($data[$key]);
				/*}*/

				/* исходная версия
				if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id')) {
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");
				
					if ($query->num_rows) {
						$url .= '/' . $query->row['keyword'];
						
						unset($data[$key]);
					}					
				} elseif ($key == 'path') {
					$categories = explode('_', $value);
					
					foreach ($categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "'");
						if ($query->num_rows) {
							$url .= '/' . $query->row['keyword'];
						}							
					}
					
					unset($data[$key]);
				*/
				}elseif($data['route'] == 'information/contact'){
					$url .= '/contact';
				}elseif($data['route'] == 'common/home'){
					$url .= '/';
				}elseif($data['route'] == 'pavblog/blogs'){
					$url .= '/blog';
				}elseif(($data['route'] == 'pavblog/blog') && ($key=='id') ){
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pavblog_blog WHERE  `blog_id`= '" . $this->db->escape((int)$value) . "'");
					$url .= '/' . $query->row['keyword'];
					unset($data[$key]);
				}elseif(($data['route'] == 'pavblog/category') && ($key=='id') ){
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pavblog_category WHERE  `category_id`= '" . $this->db->escape((int)$value) . "'");
					$url .= '/' . $query->row['keyword'];
					unset($data[$key]);
				}
			}
		}
	
		if ($url) {
			unset($data['route']);
		
			$query = '';
		
			if ($data) {
				foreach ($data as $key => $value) {
					$query .= '&' . $key . '=' . $value;
				}
				
				if ($query) {
					$query = '?' . trim($query, '&');
				}
			}
			return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
		} else {
			return $link;
		}
	}	
}
?>