<?php


$_['entry_sign_up_for_newsletter'] = "Подпишисьна новости!";

$_['entry_newsletter'] = "Новости";

$_['button_ok'] = "Ok";

$_['button_subscribe'] = "Подписаться";

$_['default_input_text'] = "Ваш email";

$_['valid_email'] = "Некорректный адрес электронной почты!";

$_['success_post'] = "Поздравляем, Вы успешно подписалисьна новости.";

$_['error_post'] = "Этот адрес электронной почты уже используется.";

?>