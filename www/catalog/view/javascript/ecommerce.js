window.dataLayer = window.dataLayer || [];
	//объявляем глобальные объекты actionField и products:
	ecommerce = {
		actionField : {
			"purchase_id":'',
			"goal_id":'',
		},
		products : [],

		//очистка
		clear_fields: function(){
			this.actionField = {
				"purchase_id":'',
				"goal_id":'',
			};
			this.products = [];
		},
		add_actionField: function(purchase_id, goal_id){
			this.actionField.purchase_id = purchase_id;
			this.actionField.goal_id = goal_id;
		},
		add_product: function(id, name, price=0, quantity=0, brand='', category=''){
			this.products.push({
				"id":id,
				"name":name,
				"price":price,
				"brand":brand,
				"category":category,
				"quantity":quantity
			});
		}
	};

	function productDetail(products){
		dataLayer.push({
		    "ecommerce": {
		        "detail": {
		            "products": products
		        }
		    }
		});
	}

	function productAddToCart(products){
		dataLayer.push({
		    "ecommerce": {
		        "add": {
		            "products": products
		            /* [
		            	{
		                    "id": data.id,
		                    "name" : data.name,
		                    "price": data.price,
		                    "brand": data.brand,
		                    "category": data.category,
		                    "quantity": data.quantity
		                }
		            ]*/
		        }
		    }
		});
	}

	function productRemove(data){
		dataLayer.push({
		    "ecommerce": {
		        "remove": {
		            "products": [
		                {
		                    "id": data.id,
		                    "name" : data.name,
		                    "category": data.category,
		                }
		            ]
		        }
		    }
		});
	}

	function productPurchase(actionField, products){
		data = {
			"ecommerce": {
		        "purchase": {
		            "actionField": {
		                "id" : actionField.purchase_id,
		                "goal_id" : actionField.goal_id
		            },
		            "products" : []
		        }
		    }
		};
		$(products).each(function(index, product){
			data.ecommerce.purchase.products.push({
				"id":product.id,
				"name":product.name,
				"price":product.price,
				"brand":product.brand,
				"category":product.category,
				"quantity":product.quantity
			});
		});

		dataLayer.push(data);
	}
	window.dataMarker = window.dataMarker || [];