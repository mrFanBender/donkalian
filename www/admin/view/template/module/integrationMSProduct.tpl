<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warnings) { ?>
        <?php foreach ($error_warnings as $error_name => $error_text) { ?>
            <div class="warning"><?php echo $error_text; ?></div>
        <?php } ?>
    <?php } ?>
    <?php if ($info_messages) { ?>
    <?php foreach ($info_messages as $info_message) { ?>
    <div class="alert alert-success"><?php echo $info_message; ?></div>
    <?php } ?>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><?php echo $text['heading_title']; ?></h1>
            <div class="buttons">
                <a class="btn btn-default" href="<?php echo $cancel; ?>"><?= $text['button_cancel']; ?></a>
            </div>
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" ><a href="<?= $index; ?>" aria-controls="IntegrationMSIndex" role="tab" data-toggle="tab">Настройки</a></li>
            <li role="presentation" class="active"><a href="<?= $integration_products; ?>" aria-controls="IntegrationMSProducts" role="tab" data-toggle="tab">Товары</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="IntegrationMSIndex">
                <div class="content">
                    <form action="<?php echo $save_settings; ?>" method="post" class="form-control">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Наименование в OpenCart</th>
                                    <th>ExternalId</th>
                                    <th>Кол-во</th>
                                    <th>Инфо</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($products as $product): ?>
                                <tr class="<?= (!$product['external_id'] ? 'danger' : '') ?>">
                                    <td><?= $product['product_id'] ?></td>
                                    <td><?= $product['name'] ?></td>
                                    <td><?= $product['external_id'] ?></td>
                                    <td><?= isset($product['stock']) ? $product['stock'] : $product['quantity'] ?></td>
                                    <td><?= $product['status'] ?></td>
                                </tr>
                                    <?php foreach ($product['modifications'] as $modification): ?>
                                        <tr class="info">
                                            <td></td>
                                            <td><?= sprintf("%s:%s",$modification['name'], $modification['value']) ?></td>
                                            <td><?= $modification['externalCode'] ?></td>
                                            <td><?= $modification['stock'] ?></td>
                                            <td><?=  'status'; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-default">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>