<?php
     $_['heading_title'] = 'Модуль интеграции с МойСклад';
    $_['integrationMS_title'] = 'Модуль интеграции с МойСклад';
    // Text
    $_['text_module']             = 'Модули';
    $_['text_success']            = 'Настройки успешно обновлены!';
    $_['button_cancel']            =    "Отмена";
    $_['button_save']            =    "Сохранить";
    // Error
    $_['error_permission']        = 'У Вас нет прав для изменения модуля "Интеграция с МойСклад"!';
    $_['integrationMS_login_empty'] = 'Заполните поле логин';
    $_['integrationMS_password_empty'] = 'Заполните поле пароль';

    //Statuses
    $_['product_not_found']        = 'Товара нет в системе МойСклад';
    $_['product_name_different']        = 'Наименование товаров в OpenCart и МойСклад различаются';

