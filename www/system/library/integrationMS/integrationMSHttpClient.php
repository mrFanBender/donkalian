<?php

require_once DIR_SYSTEM . 'library/integrationMS/bootstrap.php';

class IntegrationMSHttpClient
{
    private $baseUrl = '';
    private $answer = array(
                'curl'=>array(),
                'data' =>array());
    private $error = false;

    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;

    }

    public function exec($baseUrl)
    {
        $this->error = false;

        $curl = curl_init($baseUrl);
        if(!$curl){
            throw new IntegrationMSCurlException('Ошибка создания объекта cURL');
        }
        $credentials = "$this->login:$this->password";
        $headers = array(
            "Content-Type: application/json",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Authorization: Basic " . base64_encode($credentials)
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //dsполняем запрос
        $answer = curl_exec($curl);
        //анализируем параметры ответа
        $curlInfo = $this->getInfo($curl);

        if($curlInfo['http_code'] != '200') {
            throw new IntegrationMSCurlException(sprintf('Ошибка связи с МойСклад, код: %s', $curlInfo['http_code']));
        }
        curl_close($curl);

        return $answer;
    }

    public function getInfo($curl)
    {
        $curlInfo = curl_getinfo($curl);
        return $curlInfo;
    }

    public function testConnection()
    {
        try {
            $this->answer['data'] = $this->exec("https://online.moysklad.ru/api/remap/1.1/entity/product", $this->login, $this->password);
        } catch ( IntegrationMSCurlException $e) {
            $this->error = $e->getMessage();

            return;
        }

        return $this->answer;
    }

    public function getProducts($limit=100, $offset=0)
    {
        try {
            $this->answer['data'] = $this->exec(sprintf("https://online.moysklad.ru/api/remap/1.1/entity/assortment?limit=%s&offset=%s", $limit, $offset),
                                                $this->login,
                                                $this->password);
        } catch ( IntegrationMSCurlException $e) {
            $this->error = $e->getMessage();

            return;
        }

        return $this->answer;
    }

    public function getStocks()
    {
        try {
            $this->answer['data'] = $this->exec("https://online.moysklad.ru/api/remap/1.1/report/stock/all?limit=100", $this->login, $this->password);
        } catch ( IntegrationMSCurlException $e) {
            $this->error = $e->getMessage();

            return;
        }

        return $this->answer;
    }

    public function hasError()
    {
        if($this->error)
        {
            return true;
        }

        return;
    }

    public function getError()
    {
        return $this->error;
    }

    public function showArray($array, $otstup='')
    {
        foreach ($array as $array_element_name => $array_element) {
            echo sprintf('</br> %s %s :', $otstup, $array_element_name);
            if (is_array($array_element)) {
               $this->showArray($array_element, $otstup.'->');

               continue;
            }

            echo $array_element;
        }
    }
}