<?php
/******************************************************
 * @package Pav Product Tabs module for Opencart 1.5.x
 * @version 1.0
 * @author http://www.pavothemes.com
 * @copyright	Copyright (C) Feb 2012 PavoThemes.com <@emai:pavothemes@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
*******************************************************/
// Heading 
$_['heading_title'] = 'Новые';

// Text
$_['text_latest']  = 'Новые'; 
$_['text_mostviewed']  = 'Популярные'; 
$_['text_featured']  = 'Мы рекомендуем'; 
$_['text_bestseller']  = 'Хиты продаж'; 
$_['text_special']  = 'Акции';
$_['text_sale'] = 'Скидка';
$_['quick_view'] = 'Quick View';
?>