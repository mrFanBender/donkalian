<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<style type="text/css">
input.small-field, select.small-field {
	width: 180px;
}
.checkout_address td {
	padding:3px;
}
</style>
<div class="container">
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action;?>" method="post" enctype="multipart/form-data">
      <table cellpadding="0" cellspacing="0" width="100%">
      <tr>
      <td valign="top" align="left" width="30%">
      <table cellpadding="0" cellspacing="0" width="100%" class="checkout_address">
      <tr>
      <td valign="top" align="left" colspan="2">
      <h2><?php echo $text_checkout_payment_address; ?></h2>
      </td>
      </tr>
      
      <?php
      //var_dump($this->session->data['order_info']);
      //echo 'Ордер ид:'.( isset($this->session->data['order_id']) ? $this->session->data['order_id'] : 0);
      if(isset($this->session->data['order_id']))
      {?>
          <tr>
          <td valign="top" align="left" width="30%"><?php echo $entry_firstname; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $this->session->data['order_info']['firstname'] /*RUS было $firstname */ ?></b>
          <input type="hidden" name="firstname" value="<?php echo $this->session->data['order_info']['firstname']; ?>"/></td>
          </tr>   
      <?php
      } else
      {?>
          <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_firstname; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="firstname" value="<?php echo $firstname; ?>" class="small-field"/></td>
          </tr>
          <?php if ($error_firstname) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_firstname; ?></div><br />
          </td>
          </tr>
          <?php } ?>
      <?php }?>
      
      <!--<?php
      if(isset($this->session->data['order_id']))
      {
        echo (isset($validate_ok) ? $validate_ok : 'no_validation'); /*RUS*/
        echo (isset($im_here) ? $im_here : 'not here'); /* RUS */
      ?>
          <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_lastname; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $lastname; ?></b>
          <input type="hidden" name="lastname" value="<?php echo $lastname; ?>"/></td>
          </tr>
      <?php
      }
      else
      {?>
      	  <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_lastname; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="lastname" value="<?php echo $lastname; ?>" class="small-field"/></td>
          </tr>
          <?php if ($error_lastname) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_lastname; ?></div><br />
          </td>
          </tr>
          <?php } ?>
      <?php }?> RUS-->
      <input type="hidden" name="lastname" value="<?php echo $lastname; ?>" class="small-field"/> <!--RUS-->
      
      <?php
      if(isset($this->session->data['order_id']))
      {?>
      	  <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_email; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $this->session->data['order_info']['email']; ?></b>
          <input type="hidden" name="email" value="<?php echo $this->session->data['order_info']['email']; ?>"/></td>
          </tr>  
      <?php
      }
      else
      {?>
      	  <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_email; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="email" value="<?php echo $email; ?>" class="small-field"/></td>
          </tr>
          <?php if ($error_email) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_email; ?></div><br />
          </td>
          </tr>
      	  <?php }?>
      <?php }?>
      
      <?php
      if(isset($this->session->data['order_id']))
      {?>
       	  <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_telephone; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $this->session->data['order_info']['telephone']; ?></b>
          <input type="hidden" name="telephone" value="<?php echo $this->session->data['order_info']['telephone']; ?>"/></td>
          </tr>
      <?php
      }
      else
      {?>
      	  <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_telephone; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="telephone" value="<?php echo $telephone; ?>" class="small-field"/></td>
          </tr>
          <?php if ($error_telephone) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_telephone; ?></div><br />
          </td>
          </tr>
      	  <?php }?>
      <?php }?>
      
      <!--<?php
      if(isset($this->session->data['order_id']))
      {?>
      	  <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_fax; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $fax; ?></b>
          <input type="hidden" name="fax" value="<?php echo $fax; ?>"/></td>
          </tr>
      <?php
      }
      else
      {?>
      	  <tr>
          <td valign="top" align="left" width="30%"><span class="required">&nbsp;</span> <?php echo $entry_fax; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="fax" value="<?php echo $fax; ?>" class="small-field"/></td>
          </tr>
      <?php }?>-->
        
      <!--<?php
      if(isset($this->session->data['order_id']))
      {?>
      	  <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_company; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $company; ?></b>
          <input type="hidden" name="company" value="<?php echo $company; ?>"/></td>
          </tr>
      <?php
      }
      else
      {?>
     	  <tr>
          <td valign="top" align="left" width="30%"><span class="required">&nbsp;</span> <?php echo $entry_company; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="company" value="<?php echo $company; ?>" class="small-field"/></td>
          </tr>
      <?php }?>-->
      
      <tr>
      <td colspan="2">
      <div style="display: <?php echo (count($customer_groups) > 1 ? 'table-row' : 'none'); ?>;"> <?php echo $entry_customer_group; ?><br />
        <?php foreach ($customer_groups as $customer_group) { ?>
        <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
        <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
        <label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
        <br />
        <?php } else { ?>
        <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" />
        <label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
        <br />
        <?php } ?>
        <?php } ?>
        <br />
      </div>
      </td>
      </tr>
      
      <!--<?php
      if(isset($this->session->data['order_id']))
      {?>
      	  <tr id="company-id-display">
      	  <td valign="top" align="left" width="30%"><?php echo $entry_company_id; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $company_id; ?></b>
          <input type="hidden" name="company_id" value="<?php echo $company_id; ?>"/></td>
          </tr>
      <?php
      }
      else
      {?>
      	  <tr id="company-id-display">
          <td valign="top" align="left" width="30%"><span class="required">&nbsp;</span> <?php echo $entry_company_id; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="company_id" value="<?php echo $company_id; ?>" class="small-field"/></td>
          </tr>
      <?php } ?>-->
        
      <?php
      if(isset($this->session->data['order_id']))
      {?>
      	  <tr id="tax-id-display">
      	  <td valign="top" align="left" width="30%"><?php echo $entry_tax_id; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $tax_id; ?></b>
          <input type="hidden" name="tax_id" value="<?php echo $tax_id; ?>"/></td>
          </tr>
      <?php
      }
      else
      {?>
          <tr id="tax-id-display">
          <td valign="top" align="left" width="30%"><span id="tax-id-required" class="required">*</span> <?php echo $entry_tax_id; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="tax_id" value="<?php echo $tax_id; ?>" class="small-field"/></td>
          </tr>
      <?php } ?>

            <?php
      if(isset($this->session->data['order_id']))
      {?>
          <tr>
          <td valign="top" align="left" width="30%"><?php echo $entry_city; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $this->session->data['order_info']['city']; ?></b>
          <input type="hidden" name="city" value="<?php echo $this->session->data['order_info']['city']; ?>"/></td>
          </tr>
      <?php
      }
      else
      {?>
          <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_city; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="city" value="<?php echo $city; ?>" class="small-field"/></td>
          </tr>
          <!--<?php if ($error_city) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_city; ?></div><br />
          </td>
          </tr>
          <?php }?>-->
      <?php } ?>
      
      <?php
      if(isset($this->session->data['order_id']))
      {?>
     	  <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_address_1; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $this->session->data['order_info']['address_1']; ?></b>
          <input type="hidden" name="address_1" value="<?php echo $this->session->data['order_info']['address_1']; ?>"/></td>
          </tr>
      <?php
      }
      else
      {?>
      	  <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_address_1; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="address_1" value="<?php echo $address_1; ?>" class="small-field"/></td>
          </tr>
          <?php if ($error_address_1) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_address_1; ?></div><br />
          </td>
          </tr>
      	  <?php }?>
      <?php } ?>
      
      <!--<?php
      if(isset($this->session->data['order_id']))
      {?>
          <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_address_2; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $address_2; ?></b>
          <input type="hidden" name="address_2" value="<?php echo $address_2; ?>"/></td>
          </tr>
      <?php
      }
      else
      {?>
          <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">&nbsp;</span> <?php echo $entry_address_2; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="address_2" value="<?php echo $address_2; ?>" class="small-field"/></td>
          </tr>
          <?php if ($error_address_1) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_address_1; ?></div><br />
          </td>
          </tr>
          <?php } ?>
      <?php } ?>-->
    
      
      <!--<?php
      if(isset($this->session->data['order_id']))
      {?>
          <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_postcode; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $postcode; ?></b>
          <input type="hidden" name="postcode" value="<?php echo $postcode; ?>"/></td>
          </tr>
      <?php
      }
      else
      {?>
          <tr>
          <td valign="top" align="left" width="30%">
          <span id="payment-postcode-required" class="required">*</span> <?php echo $entry_postcode; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="postcode" value="<?php echo $postcode; ?>" class="small-field"/></td>
          </tr>
          <?php if ($error_postcode) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_postcode; ?></div><br />
          </td>
          </tr>
      	  <?php }?>
      <?php } ?>-->
      
      <!--<?php
      if(isset($this->session->data['order_id']))
      {?>
      	  <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_country; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $country_name; ?></b>
          <input type="hidden" name="country_id" value="<?php echo $country_id; ?>"/></td>
          </tr>
          
          <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_zone; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $zone_name; ?></b>
          <input type="hidden" name="zone_id" value="<?php echo $zone_id; ?>"/></td>
          </tr>
      <?php
      }
      else
      {?>
          <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_country; ?></td>
          <td valign="top" align="left" width="70%">
          <select name="country_id" class="small-field">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($countries as $country) { ?>
            <?php if ($country['country_id'] == $country_id) { ?>
            <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
          </td>
          </tr>
          <?php if ($error_country) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_country; ?></div><br />
          </td>
          </tr>
          <?php }?>
          
          <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_zone; ?></td>
          <td valign="top" align="left" width="70%">
          <select name="zone_id" class="small-field">
      	  </select>
          </td>
          </tr>
          <?php if ($error_zone) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_zone; ?></div><br />
          </td>
          </tr>
          <?php }?>
      <?php }?> RUS убрал зону и страну -->
      </table>
      <div>
      <?php
      if(!isset($this->session->data['order_id']))
      {?>
          <?php
          if($check_shipping_address == 1)
          {?>
          <input type="checkbox" name="check_shipping_address" value="1" checked="checked" class="hidden"/> <!--<?php echo $entry_shipping;?> RUS-->
          <?php 
          } else{?>
          <input type="checkbox" name="check_shipping_address" value="1"/> <?php echo $entry_shipping;?>
          <?php }?>
          </div>
      <?php 
      } else if($check_shipping_address == 1) {?>
      <br/><b><?php echo $entry_shipping;?></b>
      <?php }?>
      <!--My delivery and billing addresses are the same.-->
      <table cellpadding="0" cellspacing="0" width="100%" id="shipping-address-display" style="margin-top:10px">
      <?php
      if(isset($this->session->data['order_id']) && $check_shipping_address == 0)
      {?>
      <tr>
      <td valign="top" align="left" colspan="2">
      <h2><?php echo $text_checkout_shipping_address; ?></h2>
      </td>
      </tr>
      <?php }?>
      <?php
      if(isset($this->session->data['order_id']))
      {?>
          <?php
          if($check_shipping_address == 0)
          {?>
          <tr>
          <td valign="top" align="left" width="30%"><?php echo $entry_firstname; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $shipping_firstname; ?></b>
          <input type="hidden" name="shipping_firstname" value="<?php echo $shipping_firstname; ?>"/></td>
          </tr>   
      	  <?php
      	  }?>
      <?php
      } else
      {?>
          <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_firstname; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="shipping_firstname" value="<?php echo $shipping_firstname; ?>" class="small-field"/></td>
          </tr>
          <?php if ($error_shipping_firstname) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_shipping_firstname; ?></div><br />
          </td>
          </tr>
          <?php } ?>
      <?php }?>
      
      <?php
      if(isset($this->session->data['order_id']))
      {?>
      	  <?php
          if($check_shipping_address == 0)
          {?>
          <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_lastname; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $shipping_lastname; ?></b>
          <input type="hidden" name="shipping_lastname" value="<?php echo $shipping_lastname; ?>"/></td>
          </tr>
          <?php }?>
      <?php
      }
      else
      {?>
      	  <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_lastname; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="shipping_lastname" value="<?php echo $shipping_lastname; ?>" class="small-field"/></td>
          </tr>
          <?php if ($error_shipping_lastname) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_shipping_lastname; ?></div><br />
          </td>
          </tr>
          <?php }?>
      <?php }?>
      
      <?php
      if(isset($this->session->data['order_id']))
      {?>
       	  <?php
          if($check_shipping_address == 0)
          {?>
      	  <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_company; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $shipping_company; ?></b>
          <input type="hidden" name="shipping_company" value="<?php echo $shipping_company; ?>"/></td>
          </tr>
          <?php }?>
      <?php
      }
      else
      {?>
     	  <tr>
          <td valign="top" align="left" width="30%"><span class="required">&nbsp;</span> <?php echo $entry_company; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="shipping_company" value="<?php echo $shipping_company; ?>" class="small-field"/></td>
          </tr>
      <?php }?>
      
      <?php
      if(isset($this->session->data['order_id']))
      {?>
      	  <?php
          if($check_shipping_address == 0)
          {?>
     	  <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_address_1; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $shipping_address_1; ?></b>
          <input type="hidden" name="shipping_address_1" value="<?php echo $shipping_address_1; ?>"/></td>
          </tr>
          <?php }?>
      <?php
      }
      else
      {?>
      	  <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_address_1; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="shipping_address_1" value="<?php echo $shipping_address_1; ?>" class="small-field"/></td>
          </tr>
          <?php if ($error_shipping_address_1) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_shipping_address_1; ?></div><br />
          </td>
          </tr>
      	  <?php }?>
      <?php }?>
      
      <?php
      if(isset($this->session->data['order_id']))
      {?>
      	  <?php
          if($check_shipping_address == 0)
          {?>
          <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_address_2; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $shipping_address_2; ?></b>
          <input type="hidden" name="shipping_address_2" value="<?php echo $shipping_address_2; ?>"/></td>
          </tr>
          <?php }?>
      <?php
      }
      else
      {?>
          <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">&nbsp;</span> <?php echo $entry_address_2; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="shipping_address_2" value="<?php echo $shipping_address_2; ?>" class="small-field"/></td>
          </tr>
      <?php }?>
      
      <?php
      if(isset($this->session->data['order_id']))
      {?>
          <?php
          if($check_shipping_address == 0)
          {?>
          <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_city; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $shipping_city; ?></b>
          <input type="hidden" name="shipping_city" value="<?php echo $shipping_city; ?>"/></td>
          </tr>
          <?php }?>
      <?php
      }
      else
      {?>
      	  <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_city; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="shipping_city" value="<?php echo $shipping_city; ?>" class="small-field"/></td>
          </tr>
          <!--<?php if ($error_city) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_city; ?></div><br />
          </td>
          </tr>
      	  <?php }?>-->
      <?php }?>
      
      <?php
      if(isset($this->session->data['order_id']))
      {?>
      	  <?php
          if($check_shipping_address == 0)
          {?>
          <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_postcode; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $shipping_postcode; ?></b>
          <input type="hidden" name="shipping_postcode" value="<?php echo $shipping_postcode; ?>"/></td>
          </tr>
          <?php }?>
      <?php
      }
      else
      {?>
          <tr>
          <td valign="top" align="left" width="30%">
          <span id="payment-postcode-required" class="required">*</span> <?php echo $entry_postcode; ?></td>
          <td valign="top" align="left" width="70%"><input type="text" name="shipping_postcode" value="<?php echo $shipping_postcode; ?>" class="small-field"/></td>
          </tr>
          <?php if ($error_shipping_postcode) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_shipping_postcode; ?></div><br />
          </td>
          </tr>
      	  <?php }?>
      <?php }?>
      
      <?php
      if(isset($this->session->data['order_id']))
      {?>
      	  <?php
          if($check_shipping_address == 0)
          {?>
      	  <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_country; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $shipping_country_name; ?></b>
          <input type="hidden" name="shipping_country_id" value="<?php echo $shipping_country_id; ?>"/></td>
          </tr>
          
          <tr>
      	  <td valign="top" align="left" width="30%"><?php echo $entry_zone; ?></td>
          <td valign="top" align="left" width="70%"><b><?php echo $shipping_zone_name; ?></b>
          <input type="hidden" name="shipping_zone_id" value="<?php echo $shipping_zone_id; ?>"/></td>
          </tr>
          <?php }?>
      <?php
      }
      else
      {?>
          <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_country; ?></td>
          <td valign="top" align="left" width="70%">
          <select name="shipping_country_id" class="small-field">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($countries as $country) { ?>
            <?php if ($country['country_id'] == $shipping_country_id) { ?>
            <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
          </td>
          </tr>
          <?php if ($error_shipping_country) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_shipping_country; ?></div><br />
          </td>
          </tr>
          <?php }?>
          
          <tr>
          <td valign="top" align="left" width="30%">
          <span class="required">*</span> <?php echo $entry_zone; ?></td>
          <td valign="top" align="left" width="70%">
          <select name="shipping_zone_id" class="small-field">
      	  </select>
          </td>
          </tr>
          <?php if ($error_shipping_zone) { ?>
          <tr>
          <td valign="top" align="left" colspan="2">
          <div class="error"><?php echo $error_shipping_zone; ?></div><br />
          </td>
          </tr>
          <?php }?>
      <?php }?>
     </table>
     </td>
     <td width="20">&nbsp;</td>
     <td  valign="top" align="left">
     <table width="100%">
     <tr>
     <td valign="top" align="left" id="shipping_method" style="border-right:solid 1px #f2f2f2">
     <?php
     if(isset($this->session->data['order_id']))
     {
     ?>
     <h2><?php echo $text_shipping_method; ?></h2><br/>
     <?php echo $this->session->data['shipping_method']['title'];?>  [<?php echo $this->session->data['shipping_method']['text'];?>]<br/><br/>
     <input type="hidden" name="shipping_method" value="<?php echo $shipping_method_code; ?>"/>
     <?php
     }
     else
     {?>
        <?php if ($shipping_methods) { ?>
        <h2><?php echo $text_shipping_method; ?></h2>
        <table class="radio">
          <?php foreach ($shipping_methods as $shipping_method) { ?>
          <tr>
            <td colspan="3"><b><?php echo $shipping_method['title']; ?></b></td>
          </tr>
          <?php if (!$shipping_method['error']) { ?>
          <?php foreach ($shipping_method['quote'] as $quote) { ?>
          <tr class="highlight">
            <td><?php if ($quote['code'] == $shipping_method_code) { ?>
              <?php $code = $quote['code']; ?>
              <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" checked="checked" />
              <?php } else { ?>
              <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" />
              <?php } ?></td>
            <td><label for="<?php echo $quote['code']; ?>"><?php echo $quote['title']; ?></label></td>
            <td style="text-align: right;"><label for="<?php echo $quote['code']; ?>"><?php echo $quote['text']; ?></label></td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td colspan="3"><div class="error"><?php echo $shipping_method['error']; ?></div></td>
          </tr>
          <?php } ?>
          <?php } ?>
        </table>
        <br />
         <?php if ($error_shipping_method) { ?>
            <div class="error"><?php echo $error_shipping_method; ?></div>
         <?php } ?>
        <?php } ?>
     <?php }?>
     </td>
     <td width="20">
     </td>
     <td align="left" valign="top">
     <?php
     if(isset($this->session->data['order_id']))
     {
     ?>
     <h2><?php echo $text_payment_method; ?></h2><br/>
     <?php echo $this->session->data['payment_method']['title'];?><br/><br/>
     <input type="hidden" name="payment_method" value="<?php echo $payment_method_code; ?>"/>
     <?php
     }
     else
     {?>
     	<?php if ($payment_methods) { ?>
        <h2><?php echo $text_payment_method; ?></h2><br/>
        <table class="radio">
          <?php foreach ($payment_methods as $payment_method) { ?>
          <tr class="highlight">
            <td><?php if ($payment_method['code'] == $payment_method_code) { ?>
              <?php $code = $payment_method['code']; ?>
              <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" checked="checked" />
              <?php } else { ?>
              <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" />
              <?php } ?></td>
            <td><label for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?></label></td>
          </tr>
          <?php } ?>
        </table>
        <br />
        <?php if ($error_payment_method) { ?>
            <div class="error"><?php echo $error_payment_method; ?></div>
         <?php } ?>
    	<?php } ?>
    <?php }?>
     </td>
    </tr>

    <tr>
      <td valign="top" align="left" colspan="3">
        <div id="woc_checkout_cart"></div>
      </td>
      </tr>
      
      <tr>
      <td colspan="3">
      <?php
      if(isset($this->session->data['order_id']))
      {?>
          <?php 
          if($comment){?>
          <h2><?php echo $text_comments; ?></h2>
          <?php echo $comment;?>
          <?php }?>
      <?php
      }
      else
      {?>
      <h2><?php echo $text_comments; ?></h2>
      <textarea name="comment" rows="8" style="width: 98%;"><?php echo $comment; ?></textarea>
      <?php } ?>
      </td>
      </tr>
      
      <tr>
      <td colspan="3">
      <?php
      if(!isset($this->session->data['order_id']))
      {?>
          <?php if ($text_agree) { ?>
            <div class="buttons">
              <div class="right"><?php echo $text_agree; ?>
                <?php if ($agree) { ?>
                <input type="checkbox" name="agree" value="1" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="agree" value="1" />
                <?php } ?>
                <?php if ($error_agree) { ?>
                <div class="error"><?php echo $error_agree; ?></div>
                <?php } ?>
              </div>
            </div>
          <?php } ?>
      <?php }?>
      
    <?php
    if(!isset($this->session->data['order_id']))
    {?>
      <input type="submit" value="<?php echo $button_continue;?>" id="button-guest" class="button" />
    <?php }?>

      </td>
      </tr>
      </table>
    </td>
    </tr>
    </table>
  </form>
</div>
<?php
if(isset($this->session->data['order_id']))
{?>
<div id="checkout_submit"></div>
<?php }?>

<?php echo $content_bottom; ?></div>

<script type="text/javascript"><!--
var check_shipping_address = $('input[name=\'check_shipping_address\']').attr('checked');
if (check_shipping_address == 'checked') {
	$('#shipping-address-display').hide();
}
else {
	$('#shipping-address-display').show();
}

$('input[name=\'check_shipping_address\']').bind('click', function() {
     if($(this).is(":checked")) {
          $('#shipping-address-display').hide();
     } else {
          $('#shipping-address-display').show();
     }
});
//--></script> 

<script type="text/javascript"><!--
$('input[name=\'customer_group_id\']:checked').live('change', function() {
	var customer_group = [];
	
<?php foreach ($customer_groups as $customer_group) { ?>
	customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group["company_id_display"]; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group["company_id_required"]; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group["tax_id_display"]; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group["tax_id_required"]; ?>';
<?php } ?>	

	if (customer_group[this.value]) {
		if (customer_group[this.value]['company_id_display'] == '1') {
			$('#company-id-display').show();
		} else {
			$('#company-id-display').hide();
		}
		
		if (customer_group[this.value]['company_id_required'] == '1') {
			$('#company-id-required').show();
		} else {
			$('#company-id-required').hide();
		}
		
		if (customer_group[this.value]['tax_id_display'] == '1') {
			$('#tax-id-display').show();
		} else {
			$('#tax-id-display').hide();
		}
		
		if (customer_group[this.value]['tax_id_required'] == '1') {
			$('#tax-id-required').show();
		} else {
			$('#tax-id-required').hide();
		}	
	}
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function() {
	if (this.value == '') return;
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#payment-postcode-required').show();
			} else {
				$('#payment-postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script> 

<script type="text/javascript"><!--
$('select[name=\'shipping_country_id\']').bind('change', function() {
	if (this.value == '') return;
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'shipping_country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#payment-postcode-required').show();
			} else {
				$('#payment-postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('select[name=\'shipping_zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'shipping_country_id\']').trigger('change');
//--></script> 

<script type="text/javascript"><!--
$('#woc_checkout_cart').load('index.php?route=checkout/woc_checkout_cart');
$('input[name=\'shipping_method\']:checked').live('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/shipping_method',
		type: 'post',
		data: 'shipping_method=' + $('input[name=\'shipping_method\']:checked').attr('value'),
		dataType: 'json',
		success: function(json) {
			if (json['code']) {
			//$('#shipping_method').after(json['code']);
			$('#woc_checkout_cart').load('index.php?route=checkout/woc_checkout_cart');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
		});	
})
//--></script> 

<script type="text/javascript"><!--
$('input[name=\'payment_method\']:checked').live('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/payment_method',
		type: 'post',
		data: 'payment_method=' + $('input[name=\'payment_method\']:checked').attr('value'),
		dataType: 'json',
		success: function(json) {
			if (json['code']) {
			//$('#shipping_method').after(json['code']);
			}
						
			$('#woc_checkout_cart').load('index.php?route=checkout/woc_checkout_cart');
		},
		error: function(xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
		});	
})
//--></script> 

<?php
if(isset($this->session->data['order_id']))
{?>
<script type="text/javascript"><!--
$('#woc_checkout_cart').load('index.php?route=checkout/woc_checkout_cart');
$.ajax({
		url: 'index.php?route=checkout/checkout/checkout_submit',
		type: 'post',
		data: '',
		dataType: 'json',				
		success: function(json) {
			$('#checkout_submit').html(json['payment']);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
//--></script> 
<?php }?>

<script type="text/javascript"><!--
$('[name=\'firstname\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/payment_address',
		type: 'post',
		data: 'firstname=' + $('input[name=\'firstname\']').attr('value'),
		dataType: 'json',				
		success: function(json) {
			$('#firstname_icon').remove();			
			if (json['firstname_error']) {
				$('input[name=\'firstname\']').after('<div id="firstname_icon" class="error"><img src="catalog/view/theme/default/image/warning.png" alt="" />&nbsp;'+ json['firstname_error'] +'</div>');
			} else if (json['firstname_success']) {
				$('input[name=\'firstname\']').before('<div id="firstname_icon"></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});

$('[name=\'lastname\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/payment_address',
		type: 'post',
		data: 'lastname=' + $('input[name=\'lastname\']').attr('value'),
		dataType: 'json',				
		success: function(json) {
			$('#lastname_icon').remove();			
			if (json['lastname_error']) {
				$('input[name=\'lastname\']').after('<div id="lastname_icon" class="error"><img src="catalog/view/theme/default/image/warning.png" alt="" />&nbsp;'+ json['lastname_error'] +'</div>');
			} else if (json['firstname_success']) {
				$('input[name=\'lastname\']').before('<div id="lastname_icon"></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});

$('[name=\'email\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/payment_address',
		type: 'post',
		data: 'email=' + $('input[name=\'email\']').attr('value'),
		dataType: 'json',				
		success: function(json) {
			$('#email_icon').remove();			
			if (json['email_error']) {
				$('input[name=\'email\']').after('<div id="email_icon" class="error"><img src="catalog/view/theme/default/image/warning.png" alt="" />&nbsp;'+ json['email_error'] +'</div>');
			} else if (json['firstname_success']) {
				$('input[name=\'email\']').before('<div id="email_icon"></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});
//--></script> 
<script type="text/javascript"><!--
$('.colorbox').colorbox({
	width: 640,
	height: 480
});
//--></script> 
<?php echo $footer; ?>