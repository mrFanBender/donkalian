<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warnings) { ?>
        <?php foreach ($error_warnings as $error_name => $error_text) { ?>
            <div class="warning"><?php echo $error_text; ?></div>
        <?php } ?>
    <?php } ?>
    <?php if ($info_messages) { ?>
    <?php foreach ($info_messages as $info_message) { ?>
    <div class="alert alert-success"><?php echo $info_message; ?></div>
    <?php } ?>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><?php echo $text['heading_title']; ?></h1>
            <div class="buttons">
                <a class="btn btn-default" href="<?php echo $cancel; ?>"><?= $text['button_cancel']; ?></a>
            </div>
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="<?= $index; ?>" aria-controls="IntegrationMSIndex" role="tab" data-toggle="tab">Настройки</a></li>
            <li role="presentation"><a href="<?= $integration_products; ?>" aria-controls="IntegrationMSProducts" role="tab" data-toggle="tab">Товары</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="IntegrationMSIndex">
                <div class="content">
                    <form action="<?php echo $save_settings; ?>" method="post" class="form-control">
                        <hr/>
                        <div class="form-group">
                            <label for="integrationMS_login">Логин МойСклад</label>
                            <input type="email" class="form-control" id="integrationMS_login" name="integrationMS_login" placeholder="Логин МойСклад" value="<?= $login ?>">
                        </div>
                        <div class="form-group">
                            <label for="integrationMS_password">Пароль от МойСклад</label>
                            <input type="password" class="form-control" id="integrationMS_password" name="integrationMS_password" placeholder="Пароль от МойСклад" value="<?= $password ?>">
                        </div>

                        <button type="submit" class="btn btn-default">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>