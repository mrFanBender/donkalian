<?php

require_once DIR_SYSTEM . 'library/integrationMS/bootstrap.php';

class ModelModuleIntegrationMSHttp extends Model
{
    protected $data = array();
    protected $offset = 0;
    protected $limit = 100;
    protected $msCrudeProducts = array();
    protected $msProducts = array();

    /**
     * @param $httpClient
     * @return mixed
     */
    protected function getCrudeMSProductsWithOffset($httpClient)
    {
        $httpData = $httpClient->getProducts($this->limit, $this->offset);
        $data = json_decode($httpData['data'], true);
        $this->offset += $this->limit;
        $this->msCrudeProducts = array_merge($this->msCrudeProducts, $data['rows']);

        return $data;
    }

    /**
     * @param $msCrudeProduct
     */
    protected function msProductPrepare($msCrudeProduct)
    {
        if ($msCrudeProduct['meta']['type'] == 'product') {
            $product = array(   'name'          =>  strtolower($msCrudeProduct['name']),
                'externalCode'  =>  $msCrudeProduct['externalCode'],
                'salePrice'     =>  $msCrudeProduct['salePrices'][0]['value'],
                'stock'         =>  $msCrudeProduct['stock'],
                'modificationsCount' =>  $msCrudeProduct['modificationsCount'],
                'modifications' =>  array());
            array_push($this->msProducts, $product);
        } elseif ($msCrudeProduct['meta']['type'] == 'variant') {
            $product = array_pop($this->msProducts);
            $modification = array(  'name'    =>  $msCrudeProduct['characteristics'][0]['name'],
                'value'     =>  $msCrudeProduct['characteristics'][0]['value'],
                'externalCode'  =>  $msCrudeProduct['externalCode'],
                'stock'     =>  $msCrudeProduct['stock']);
            $product['stock']+=$modification['stock'];

            array_push($product['modifications'], $modification);
            array_push($this->msProducts, $product);
        }

    }
    /**
     * get assortments(products, modifications and quantity from МойСклад and
     */
    public function getMSProducts($login, $pass)
    {
        $httpClient = new IntegrationMSHttpClient($login, $pass);
        $this->msCrudeProducts = array();
        $this->msProducts = array();
        $this->offset = 0;

        //собираем данные, полученные от МС, в один массив
        do {
            $data = $this->getCrudeMSProductsWithOffset($httpClient);
        } while (isset($data['meta']['nextHref']));

        //Преобразуем в более удобный вид
        foreach ($this->msCrudeProducts as $msCrudeProduct) {
            $this->msProductPrepare($msCrudeProduct);
        }

        return $this->msProducts;
    }


}