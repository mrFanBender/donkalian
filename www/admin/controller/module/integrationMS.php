<?php

require_once DIR_SYSTEM . 'library/integrationMS/bootstrap.php';


class ControllerModuleIntegrationMS extends Controller
{
    private $error = array();
    /**
     * Module Settings
     *
     * @var array
     */
    private $settings = array();

    /*public function __construct()
    {

    }*/


    public function integrationParamsInit()
    {
        $this->load->model('setting/setting');
        $this->load->model('module/integrationMS');
        $this->load->language('module/integrationMS');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('/admin/view/stylesheet/integrationMS.css');
        $this->document->addStyle('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
        $this->document->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');

        $this->settings = $this->model_setting_setting->getSetting('integrationMS');
        $this->data['module_settings'] = $this->settings;

        //Загрузка текстовых переменных
        $this->data['text'] = array();

        $textParameters = array(
            'heading_title',
            'button_cancel',
            'button_save',
            'text_home',
            'text_module',
        );
        foreach ($textParameters as $textParameter) {
            $this->data['text'][$textParameter] = $this->language->get($textParameter);
        }

        //хлебные крошки
        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link(
                'common/home',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link(
                'extension/module',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('integrationMS_title'),
            'href'      => $this->url->link(
                'module/integrationMS',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => ' :: '
        );

        //Ссылки
        $this->data['index'] = $this->url->link(
            'module/integrationMS',
            'token=' . $this->session->data['token'], 'SSL'
        );
        $this->data['cancel'] = $this->url->link(
            'extension/module',
            'token=' . $this->session->data['token'], 'SSL'
        );
        $this->data['save_settings'] = $this->url->link(
            'module/integrationMS/saveSettings',
            'token=' . $this->session->data['token'], 'SSL'
        );
        $this->data['integration_products'] = $this->url->link(
            'module/integrationMS/integrationProducts',
            'token=' . $this->session->data['token'], 'SSL'
        );

        //Errors
        $this->data['error_warnings'] = array();
        if(isset($this->session->data['errors'])){
            $this->data['error_warnings']=$this->session->data['errors'];
            $this->session->data['errors'] = array();
        }

        //Info Messages
        $this->data['info_messages'] = array();
        if(isset($this->session->data['success'])){
            $this->data['info_messages'] = $this->session->data['success'];
            $this->session->data['success'] = array();
        }
    }

    /**
     * Show main module page
     */
    public function index()
    {
        $this->integrationParamsInit();

        $settings = $this->model_setting_setting->getSetting('integrationMS');
        $this->data['login'] = $settings['integrationMS_login'];
        $this->data['password'] = $settings['integrationMS_pass'];

        $this->load->model('design/layout');
        $_data['layouts'] = $this->model_design_layout->getLayouts();

        $this->template = 'module/integrationMS.tpl';
        $this->children = array(
            'common/header',
            'common/footer',
        );

        $this->response->setOutput($this->render());
    }

    public function saveSettings()
    {
        $this->load->model('setting/setting');
        $this->load->language('module/integrationMS');

        $this->session->data['errors'] = array();

        if (empty($this->request->post['integrationMS_login'])) {
            //$this->data['error_warnings']['integrationMS_login'] = $this->language->get('integrationMS_login_empty');
            $this->session->data['errors']['integrationMS_login'] = $this->language->get('integrationMS_login_empty');
        }
        if (empty($this->request->post['integrationMS_password'])) {
            //$this->data['error_warnings']['integrationMS_password'] = $this->language->get('integrationMS_password_empty');
            $this->session->data['errors']['integrationMS_password'] = $this->language->get('integrationMS_password_empty');
        }

        if($this->session->data['errors']){
            $this->redirectToMethod();
        }

        $settings = $this->model_setting_setting->getSetting('integrationMS');
        $settings['integrationMS_login'] = $this->request->post['integrationMS_login'];
        $settings['integrationMS_pass'] = $this->request->post['integrationMS_password'];

        //Проверка связи с МойСклад
        $httpClient = new IntegrationMSHttpClient($settings['integrationMS_login'], $settings['integrationMS_pass']);
        if (!$httpClient->testConnection()) {
            $this->session->data['errors']['integrationMS_connection'] = $httpClient->getError();
            $this->redirectToMethod();
        }

        $this->model_setting_setting->editSetting('integrationMS', $settings);
        $this->session->data['success']['connection_ok'] = $this->language->get('text_success');
        //$this->data['info_messages']['text_success'] = $this->language->get('text_success');

        $this->redirectToMethod();
    }

    public function integrationProducts()
    {
        $this->integrationParamsInit();
        $this->load->model('module/integrationMSHttp');

        //Errors
        $this->data['error_warnings'] = array();
        if(isset($this->session->data['errors'])){
            $this->data['error_warnings']=$this->session->data['errors'];
            $this->session->data['errors'] = array();
        }

        $settings = $this->model_setting_setting->getSetting('integrationMS');
        $this->data['login'] = $settings['integrationMS_login'];
        $this->data['password'] = $settings['integrationMS_pass'];

        //загрузка товаров из OC
        $productsOC = $this->model_module_integrationMS->getIntegrationProducts();

        //загрузка товаров из МС
        $productsMS = $this->model_module_integrationMSHttp->getMSProducts($settings['integrationMS_login'], $settings['integrationMS_pass']);

        $notSynchronized = 0;

        foreach ($productsOC as &$product) {
            $product['modifications'] = array();

            foreach ($productsMS as $productMS) {
                if ($product['name'] != $productMS['name']) {
                    continue;
                }

                $product['external_id'] = $productMS['externalCode'];
                $product['modifications'] = $productMS['modifications'];
                $quantity = 0;
                foreach ($product['modifications'] as $modification) {
                    $quantity +=$modification['stock'];
                }
                $product['quantity'] = $quantity ? $quantity : $productMS['stock'];
                continue 2;
            }
            $notSynchronized++;
        }

        if ($notSynchronized > 0) {
            $this->data['error_warnings'][] = sprintf("Имеются Несинхронизированные позиции. Это происходит из-за того, что названия товаров в системе МойСклад и в OpenCart не совпадают. Кол-во несинхронизированных позиций:%s", $notSynchronized);
        }

        //заносим товары в систему
        $this->model_module_integrationMS->updateIntegrationProducts($productsOC);

        $this->model_module_integrationMS->updateIntegrationOptions($productsOC);

        $this->data['products'] = $productsOC;
        $this->load->model('design/layout');
        $_data['layouts'] = $this->model_design_layout->getLayouts();

        $this->template = 'module/integrationMSProduct.tpl';
        $this->children = array(
            'common/header',
            'common/footer',
        );

        $this->response->setOutput($this->render());
    }

    public function integrationLog()
    {

    }

    public function redirectToMethod($url='')
    {
        $fullUrl = 'module/integrationMS';
        if ($url) {
            if (!strpos($url, '/')) {
                $url='/'.$url;
            }
            $fullUrl.=$url;
        }
        $redirect = $this->url->link(
            $fullUrl, 'token=' . $this->session->data['token'],
            'SSL'
        );

        $this->redirect($redirect);
    }

    /**
     * Cron synchronization action
     */
    public function synchronize()
    {
        $log = new Log('integrationMS.txt');
        $log->write(sprintf('%s: Синхронизация запущена', date('d-m-Y H-i-s')));

        $this->load->model('setting/setting');
        $this->load->model('module/integrationMS');
        $this->load->language('module/integrationMS');
        $this->load->model('module/integrationMSHttp');

        $settings = $this->model_setting_setting->getSetting('integrationMS');
        $this->data['login'] = $settings['integrationMS_login'];
        $this->data['password'] = $settings['integrationMS_pass'];

        //Errors
        $this->data['error_warnings'] = array();

        //Получаем список товаров OpenCart и их externalId
        $productsOC = $this->model_module_integrationMS->getIntegrationProducts();
        $log->write(sprintf('%s: --Получен список товаров из системы', date('d-m-Y H-i-s')));

        //получаем список товарных позиций из MS
        $productsMS = $this->model_module_integrationMSHttp->getMSProducts($settings['integrationMS_login'], $settings['integrationMS_pass']);
        $log->write(sprintf('%s: --Получен список товаров из МойСклад', date('d-m-Y H-i-s')));

        //проводим сравнение, готовим массив для сохранения
        $notSynchronized = 0;

        foreach ($productsOC as &$product) {
            $product['modifications'] = array();

            foreach ($productsMS as $productMS) {
                if ($product['name'] != $productMS['name']) {
                    continue;
                }

                $product['external_id'] = $productMS['externalCode'];
                $product['modifications'] = $productMS['modifications'];
                $quantity = 0;
                foreach ($product['modifications'] as $modification) {
                    $quantity +=$modification['stock'];
                }
                $product['quantity'] = $quantity ? $quantity : $productMS['stock'];
                continue 2;
            }
            $notSynchronized++;
        }

        //Обновляем товарные остатки
        $this->model_module_integrationMS->updateIntegrationProducts($productsOC);
        $log->write(sprintf('%s: --Товарные остатки обновлены', date('d-m-Y H-i-s')));

        //Обновляем товарные опции
        $this->model_module_integrationMS->updateIntegrationOptions($productsOC);
        $log->write(sprintf('%s: -- Товарные опции обновлены', date('d-m-Y H-i-s')));

        if($notSynchronized > 0) {
            $log->write(sprintf('%s: --Внимание, есть несинхронизированные позиции. Кол-во: %s', date('d-m-Y H-i-s'), $notSynchronized));
        }

    }

    public function install()
    {
        $this->load->model('setting/setting');
        $this->load->model('module/integrationMS');
        //Подготовка БД для работы модуля
        $this->model_module_integrationMS->createTables();

        //инициализация настроек модуля
        $settings = $this->model_setting_setting->getSetting('integrationMS');
        $settings['integrationMS_show_menu'] = 1;
        $settings['integrationMS_login'] = '';
        $settings['integrationMS_pass'] = '';
        $settings['integrationMS_active_link'] = 0;
        $this->model_setting_setting->editSetting('integrationMS', $settings);
    }

    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->load->model('module/integrationMS');

        $this->model_module_integrationMS->dropTables();

        $settings = $this->model_setting_setting->getSetting('integrationMS');
        $settings['integrationMS_show_menu'] = 0;
        $settings['integrationMS_login'] = '';
        $settings['integrationMS_pass'] = '';
        $settings['integrationMS_active_link'] = 0;
        $this->model_setting_setting->editSetting('integrationMS', $settings);
    }

}