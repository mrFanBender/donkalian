<?php
class ControllerFeedYmlFileGenerator extends Controller {
	public function index() {
		$output  = '<?xml version="1.0" encoding="utf-8"?>';
		$output .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">';
		$this->load->model('catalog/product');
		$this->load->model('catalog/category');

		$output .= '<yml_catalog date="'.date("Y-m-d H:i").'">';
		$output .= '<shop>';
		$output .= '<name>'.$this->config->get('config_name').'</name>
		    	<company>'.$this->config->get('config_title').'</company>';
		$output .= '<url>'./*$this->config->get('url')*/'donkalian.ru'.'</url>
		    	<currencies>
					<currency id="RUR" rate="1"/>
		    	</currencies>';
		//категории магазина
		$output .= '<categories>';
		$categories = $this->model_catalog_category->getAllCategories();
		foreach($categories as $category){
			if($category['parent_id'] == 0){
				$output .= '<category id="'.$category['category_id'].'">'.$category['name'].'</category>';
			}else{
				$output .= '<category id="'.$category['category_id'].'" parentId="'.$category['parent_id'].'">'.$category['name'].'</category>';
			}
		}
		$output .= '</categories>';
		/*
		    <delivery-options> ... </delivery-options>
		    <offers> ... </offers>*/

		//товары магазина
		$output .= '<offers>';
		$products = $this->model_catalog_product->getProducts();
		foreach ($products as $product) {
			//полный путь, включающий категории
			$main_category_id = $this->model_catalog_product->getProductMainCategory($product['product_id']);
			$categories_id = $this->model_catalog_category->getPath($main_category_id);
			$path='';			
			if($categories_id){
				foreach($categories_id as $category_id){
					$path .=$category_id['path_id'].'_' ;
				}
				if(strlen($path)>0){
					$path=substr($path,0,-1);
				}
			}
			$output .= '<offer id="'.$product['product_id'].'" type="vendor.model" available="true">';
			$output .= '<url>'.$this->url->link('product/product','path='.$path.'&product_id='.$product['product_id']).'</url>';
			$output .= '<price>'.$product['price'].'</price>';
			$output .= '<currencyId>RUR</currencyId>';
			$output .= '<vendor>'.$product['manufacturer'].'</vendor>';
			$output .= '<model>'.$product['name'].'</model>';
			$product_description = $product['description'];
			//$product_description = str_ireplace('&nbsp','1', $product_description);
    			$output .= '<description>'./*strip_tags(html_entity_decode*/$product_description/*htmlspecialchars($product['description'])*/.'</description>';
			$output .= '</offer>';
		}
		$output .= '</offers>';
		$output .= '</shop>';
		$output .= '</yml_catalog>';

		$this->response->addHeader('Content-Type: application/xml');
		/*$this->response->addHeader('<?xml version="1.0" encoding="utf-8"?><!DOCTYPE yml_catalog SYSTEM "shops.dtd">');*/
		$this->response->setOutput($output);
		//$this->response->setOutput(var_dump($products));
	}
}
?>