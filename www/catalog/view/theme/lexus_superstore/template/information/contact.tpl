<?php require( DIR_TEMPLATE.$this->config->get('config_template')."/template/common/config.tpl" );
    $themeConfig = $this->config->get( 'themecontrol' );
?>
<?php echo $header; ?>
<?php require( DIR_TEMPLATE.$this->config->get('config_template')."/template/common/breadcrumb.tpl" );  ?>

<div class="container">
<div class="row">

	<?php if( $SPAN[0] ): ?>
	<aside class="col-lg-<?php echo $SPAN[0];?> col-md-<?php echo $SPAN[0];?> col-sm-12 col-xs-12">
		<?php echo $column_left; ?>
	</aside>
	<?php endif; ?> 

	<section class="col-lg-<?php echo $SPAN[1];?> col-md-<?php echo $SPAN[1];?> col-sm-12 col-xs-12">  
		<div id="content">
			<?php echo $content_top; ?>			
			<h1><?php echo $text_location; ?></h1>
			<div class="wrapper no-margin">

<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3AHVV42jOIEizagsFi7IRWczoX2YuhZBtH&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
	            <!--<div class="contact-location hidden-xs">

	                <div id="contact-map">
	                	
	                </div>
	            </div>-->
				<div class="row contact-content">					
					<?php  /*if(  isset($themeConfig['contact_customhtml']) && isset($themeConfig['contact_customhtml'][$this->config->get('config_language_id')]) && trim($themeConfig['contact_customhtml'][$this->config->get('config_language_id')]) ){ */?>
						<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 rus-contact-box">
							<div class="contact-info">
								<h3><?php echo $heading_title; ?></h3>
								<div class="content">
									<div class="contact-row">
										<p class="contact-row-title"><span class="fa fa-flag"></span><span class=""><?= $text_firm_name ?></span></p>
										<p class="contact-row-value"><?= $store ?></p>
									</div>
									<div class="contact-row">
										<p class="contact-row-title"><span class="fa fa-phone"></span><span class=""><?= $text_telephone ?></span></p>
										<p class="contact-row-value"><?= "+7-902-502-38-19" ?></p>
									</div>
									<div class="contact-row">
										<p class="contact-row-title"><span class="fa fa-envelope"></span><span class=""><?= $text_email ?></span></p>
										<p class="contact-row-value"><?= "donkalian@yandex.ru" ?></p>
									</div>
									<div class="contact-row">
										<p class="contact-row-title"><span class="fa fa-clock-o"></span><span class="">Режим работы:</span></p>
										<p class="contact-row-value">прием заказов каждый день: с 9:00 до 23:00</p>
									</div>
									<div class="contact-row">
										<p class="contact-row-title"><span class="fa fa-home"></span><span class=""><?= $text_address ?></span></p>
										<p class="contact-row-value"><?= $address ?></p>
									</div>
									<!--<div class="contact-row">
										<p class="contact-row-title"><span class="fa fa-credit-card"></span><span class=""><?= $text_inn ?></span></p>
										<p class="contact-row-value"><?= "6670422792" ?></p>
									</div>
									<div class="contact-row">
										<p class="contact-row-title"><span class="fa fa-credit-card"></span><span class=""><?= $text_kpp ?></span></p>
										<p class="contact-row-value"><?= "667001001" ?></p>
									</div>
									<div class="contact-row">
										<p class="contact-row-title"><span class="fa fa-credit-card"></span><span class=""><?= $text_ogrn ?></span></p>
										<p class="contact-row-value"><?= "1146670009529" ?></p>
									</div>
									<div class="contact-row">
										<p class="contact-row-title"><span class="fa fa-credit-card"></span><span class=""><?= $text_bik ?></span></p>
										<p class="contact-row-value"><?= '046577964 (ОАО«Альфа-Банк»)' ?></p>
									</div>
									<div class="contact-row">
										<p class="contact-row-title"><span class="fa fa-credit-card"></span><span class=""><?= $text_r_sch ?></span></p>
										<p class="contact-row-value"><?= "40702810338060001103" ?></p>
									</div>
									<div class="contact-row">
										<p class="contact-row-title"><span class="fa fa-credit-card"></span><span class=""><?= $text_k_sch ?></span></p>
										<p class="contact-row-value"><?= "30101810100000000964" ?></p>
									</div>-->

								</div>	
							</div>
							<div class="contact-customhtml">
								<div class="content">
									<?php echo html_entity_decode($themeConfig['contact_customhtml'][$this->config->get('config_language_id')]); ?>
								</div>
							</div>
						</div>
					<?php /*} */?>

					<div class="col-lg-7 col-md-7 col-sm-12">
						<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" role="form">				
							<h3><?php echo $text_contact; ?></h3>
							<div class="content">
								<div class="form-group">
									<label><?php echo $entry_name; ?></label>
									<p><input type="text" name="name" value="<?php echo $name; ?>" class="form-control" /></p>
								</div>							
								<?php if ($error_name) { ?>
								<p><span class="error"><?php echo $error_name; ?></span></p>
								<?php } ?>
						
								<div class="form-group">
									<label><?php echo $entry_email; ?></label>
									<p><input type="text" name="email" value="<?php echo $email; ?>" class="form-control" /></p>
								</div>
								<?php if ($error_email) { ?>
								<p><span class="error"><?php echo $error_email; ?></span></p>
								<?php } ?>
						
								<div class="form-group">
									<label><?php echo $entry_enquiry; ?></label>
									<p>
										<textarea name="enquiry" cols="50" rows="10" class="form-control"><?php echo $enquiry; ?></textarea>
									</p>
								</div>
								<?php if ($error_enquiry) { ?>
								<p><span class="error"><?php echo $error_enquiry; ?></span></p>
								<?php } ?>
						
								<div class="form-group">
									<label><?php echo $entry_captcha; ?></label>
									<p><input type="text" name="captcha" value="<?php echo $captcha; ?>" class="form-control" /></p>
									<img src="index.php?route=information/contact/captcha" alt="" class="img-responsive img-captcha" />
								</div>
								<?php if ($error_captcha) { ?>
								<p><span class="error"><?php echo $error_captcha; ?></span></p>
								<?php } ?>
							</div>
							
							<div class="buttons">
								<div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="button btn btn-theme-default" /></div>
							</div>				
						</form>
					</div>
				</div>


				

	            <!--<?php // Jquery googlemap api v3?>
	            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&language=en"></script>
	            <script type="text/javascript" src="catalog/view/javascript/gmap/gmap3.min.js"></script>
	            <script type="text/javascript" src="catalog/view/javascript/gmap/gmap3.infobox.js"></script>
	            <script type="text/javascript">
	                var mapDiv, map, infobox;
	                var lat = <?php echo isset($themeConfig['location_latitude'])?$themeConfig['location_latitude']:'40.705423'; ?>;
	                var lon = <?php echo isset($themeConfig['location_longitude'])?$themeConfig['location_longitude']:'-74.008616'; ?>;
	                jQuery(document).ready(function($) {
	                    mapDiv = $("#contact-map");
	                    mapDiv.height(400).gmap3({
	                        map:{
	                            options:{
	                                center:[lat,lon],
	                                zoom: 15
	                            }
	                        },
	                        marker:{
	                            values:[
	                                {latLng:[lat, lon], data:"<?php echo isset($themeConfig['location_address'])?$themeConfig['location_address']:'79-99 Beaver Street, New York, NY 10005, USA'; ?>"},
	                            ],
	                            options:{
	                                draggable: false
	                            },
	                            events:{
	                                  mouseover: function(marker, event, context){
	                                    var map = $(this).gmap3("get"),
	                                        infowindow = $(this).gmap3({get:{name:"infowindow"}});
	                                    if (infowindow){
	                                        infowindow.open(map, marker);
	                                        infowindow.setContent(context.data);
	                                    } else {
	                                        $(this).gmap3({
	                                        infowindow:{
	                                            anchor:marker, 
	                                            options:{content: context.data}
	                                        }
	                                      });
	                                    }
	                                },
	                                mouseout: function(){
	                                    var infowindow = $(this).gmap3({get:{name:"infowindow"}});
	                                    if (infowindow){
	                                        infowindow.close();
	                                    }
	                                }
	                            }
	                        }
	                    });
	                });
	            </script>
	            <?php //end contact map ?>-->
			</div>
			<?php echo $content_bottom; ?>
		</div>
	</section> 

	<?php if( $SPAN[2] ): ?>
	<aside class="col-lg-<?php echo $SPAN[2];?> col-md-<?php echo $SPAN[2];?> col-sm-12 col-xs-12">	
		<?php echo $column_right; ?>
	</aside>
	<?php endif; ?>

</div></div>

<?php echo $footer; ?>