<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

class ControllerModuleProductimport extends Controller {
	private $error = array();
	/** Error reporting */
	/*error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	date_default_timezone_set('Europe/London');

	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');*/

	public function index() {
		$this->language->load('module/productimport');
		$this->document->setTitle($this->language->get('heading_title')); 
		$this->load->model('catalog/product2');
		
		//данные для шаблона
		$this->data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
		$this->data['entry_category'] = $this->language->get('entry_category');
		$this->data['entry_main_category'] = $this->language->get('entry_main_category');
		$this->data['entry_weight_class'] = $this->language->get('entry_weight_class');
		$this->data['entry_dimension'] = $this->language->get('entry_dimension');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['warning_select_image'] = $this->language->get('warning_select_image');
		//session token
		$this->data['token'] = $this->session->data['token'];
		//image
		$this->load->model('tool/image');
		$this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		//image
		$this->data['image'] = '';
		//manufacturer
		if (isset($this->request->post['manufacturer_id'])) {
			$this->data['manufacturer_id'] = $this->request->post['manufacturer_id'];
		} elseif (!empty($product_info)) {
			$this->data['manufacturer_id'] = $product_info['manufacturer_id'];
		} else {
			$this->data['manufacturer_id'] = 0;
		}

		if (isset($this->request->post['manufacturer'])) {
			$this->data['manufacturer'] = $this->request->post['manufacturer'];
		} elseif (!empty($product_info)) {
			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($product_info['manufacturer_id']);

			if ($manufacturer_info) {		
				$this->data['manufacturer'] = $manufacturer_info['name'];
			} else {
				$this->data['manufacturer'] = '';
			}	
		} else {
			$this->data['manufacturer'] = '';
		}
		// Categories
		$this->load->model('catalog/category');

		if (isset($this->request->post['product_category'])) {
			$categories = $this->request->post['product_category'];
		} elseif (isset($this->request->get['product_id'])) {		
			$categories = $this->model_catalog_product->getProductCategories($this->request->get['product_id']);
		} else {
			$categories = array();
		}

		$this->data['product_categories'] = array();

		foreach ($categories as $category_id) {
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$this->data['product_categories'][] = array(
					'category_id' => $category_info['category_id'],
					'name'        => ($category_info['path'] ? $category_info['path'] . ' &gt; ' : '') . $category_info['name']
				);
			}
		}
		// MAIN CATEGORY
		$this->load->model('catalog/category');

		if (isset($this->request->post['main_category_id'])) {
			$main_category_id = $this->request->post['main_category_id'];
		} elseif (isset($this->request->get['product_id'])) {		
			$main_category_id = $this->model_catalog_product->getProductMainCategory($this->request->get['product_id']);
		} else {
			$main_category_id = 0;
		}

		$this->data['main_category'] = array(
				'category_id' => '',
				'name'=> '');

		$main_category_info = $this->model_catalog_category->getCategory($main_category_id);

		if ($main_category_info) {
			$this->data['main_category'] = array(
				'category_id' => $main_category_info['category_id'],
				'name'        => ($main_category_info['path'] ? $main_category_info['path'] . ' &gt; ' : '') . $main_category_info['name']
			);
		}
		/** Include PHPExcel */
		if(isset($_FILES['product_list'])){
			require_once DIR_APPLICATION.'/Classes/PHPExcel.php';
			try{
				$inputFileName = $_FILES['product_list']['tmp_name'];
				/**  Identify the type of $inputFileName  **/
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				/**  Create a new Reader of the type that has been identified  **/
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				/**  Load $inputFileName to a PHPExcel Object  **/
				$objPHPExcel = $objReader->load($inputFileName);
				echo 'получили библиотеку и загрузили файл.<br/>-------------------------------------------------------------<br/>';
				if(isset($this->request->post['image']) && $this->request->post['image'] != '') {
					$this->data['image'] = $this->request->post['image'];
				}else{
					$this->error['warning'] = $this->data['warning_select_image'];
					throw new Exception($this->data['warning_select_image']);
				}

				if(isset($this->request->post['product_category'])){
					var_dump($this->request->post['product_category']);
				}
				//массив товаров
				$products = array($this->request->post['category']);
				//массив букв
				$char_array = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
				$parameter = '';
				$row = 2;
				$col = 0;
				while(!is_null($objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue())){
					while(!is_null($parameter = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].'1')->getValue())){
						if($parameter=='product_related'){
							$products[$row-2][$parameter] = array();
							$products[$row-2][$parameter] = explode(';', $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue());	
						}elseif($parameter=='product_special_priority'){
							$products[$row-2]['product_special'][0]['priority'] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
						}elseif($parameter=='product_special_customer_group_id'){
							$products[$row-2]['product_special'][0]['customer_group_id'] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
						}elseif($parameter=='product_special_price'){
							$products[$row-2]['product_special'][0]['price'] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
						}elseif($parameter=='product_special_date_start'){
							$products[$row-2]['product_special'][0]['date_start'] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
						}elseif($parameter=='product_special_date_end'){
							$products[$row-2]['product_special'][0]['date_end'] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
						}else{
							$products[$row-2][$parameter] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
						}
							//$products[$row-2]['isbn'] = '';
						//$products[$row-2][$parameter] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
						$col++;
					}
					//добавляем стандартные параметры
					$products[$row-2]['upc'] = '';
					$products[$row-2]['ean'] = '';
					$products[$row-2]['mpn'] = '';
					$products[$row-2]['jan'] = '';
					$products[$row-2]['isbn'] = '';
					$products[$row-2]['location'] = '';
					$products[$row-2]['minimum'] = 1;
					$products[$row-2]['date_available'] = date('Y-m-d');
					$products[$row-2]['shipping'] = 1;
					$products[$row-2]['length'] = '';
					$products[$row-2]['weight'] = '';
					$products[$row-2]['width'] = '';
					$products[$row-2]['height'] = '';
					$products[$row-2]['subtract'] = '';
					$products[$row-2]['points'] = 0;
					$products[$row-2]['image'] = $this->data['image'];
					$products[$row-2]['manufacturer'] = $this->data['manufacturer_id'];
					continue;
					var_dump($categories);
					$products[$row-2]['product_category'] = $categories;
					var_dump($main_category_id);
					$products[$row-2]['main_category_id'] = $main_category_id;

					$row++;
					$col=0;
				}
				var_dump($products);

				foreach($products as $product){
					$this->model_catalog_product2->addProduct($product);
				}
			}catch(PHPExcel_Reader_Exception $e){
				$this->error['warning'] = $e->getMessage();
			}catch(Exception $e){
				$this->error['warning'] = $this->data['warning_select_image'];
			}
		}


		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$url = '';
		//шаблон
		$this->template = 'module/productimport.tpl';
		$this->children = array(
			'common/header',
			'common/footer');
		$this->response->setOutput($this->render());
	}



	public function index2() {
		$this->language->load('catalog/product');

		$this->document->setTitle($this->language->get('heading_title')); 

		$this->load->model('catalog/product');
		var_dump('удалось загрузиться');die;
		//$this->getList();



		try{
			$inputFileName = $_FILES['product_list']['tmp_name'];
			/**  Identify the type of $inputFileName  **/
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			/**  Create a new Reader of the type that has been identified  **/
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			/**  Load $inputFileName to a PHPExcel Object  **/
			$objPHPExcel = $objReader->load($inputFileName);
		}catch(PHPExcel_Reader_Exception $e){
			die('Error loading file: '.$e->getMessage());
		}
		//массив товаров
		$products = array();
		//массив букв
		$char_array = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
		//var_dump($objPHPExcel);
		echo '-------------------------------------- <br/>';
		$parameter = '';
		$row = 2;
		$col = 0;
		while(!is_null($objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue())){
			while(!is_null($parameter = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].'1')->getValue())){
				if($parameter=='product_category' || $parameter=='product_related'){
					$products[$row-2][$parameter] = explode(';', $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue());	
				}elseif($parameter=='product_special_priority'){
					$products[$row-2]['product_special'][0]['priority'] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
				}elseif($parameter=='product_special_customer_group_id'){
					$products[$row-2]['product_special'][0]['customer_group_id'] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
				}elseif($parameter=='product_special_price'){
					$products[$row-2]['product_special'][0]['price'] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
				}elseif($parameter=='product_special_date_start'){
					$products[$row-2]['product_special'][0]['date_start'] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
				}elseif($parameter=='product_special_date_end'){
					$products[$row-2]['product_special'][0]['date_end'] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
				}else{
					$products[$row-2][$parameter] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
				}
				//$products[$row-2][$parameter] = $objPHPExcel->getActiveSheet()->getCell($char_array[$col].$row)->getValue();
				$col++;
			}
			$row++;
			$col=0;
		}
		var_dump($products);
		//var_dump($objPHPExcel->getActiveSheet()->getCell('A1')->getValue());
	}
}