<?php


class ModelModuleIntegrationMS extends Model
{
    public function createTables()
    {
        $this->createIntegrationProductTable();
        $this->createIntegrationProductOptionTable();
    }

    public function createIntegrationProductTable()
    {
        $query = $this->db->query("
            CREATE TABLE IF NOT EXISTS `".DB_PREFIX."integrationms_product` (
            `id` INT AUTO_INCREMENT, 
            `product_id` INT NOT NULL, 
            `external_id` VARCHAR(40), 
            PRIMARY KEY(id))");
    }

    public function createIntegrationProductOptionTable()
    {
        $query = $this->db->query("
            CREATE TABLE IF NOT EXISTS `".DB_PREFIX."integrationms_product_option` (
            `id` INT AUTO_INCREMENT, 
            `product_option_value_id` INT NOT NULL, 
            `external_id` VARCHAR(40), 
            PRIMARY KEY(id))");
    }

    public function dropTables()
    {
        $this->dropIntegrationProductTable();
        $this->dropIntegrationProductOptionTable();
    }

    public function dropIntegrationProductTable()
    {
        $query = $this->db->query("
            DROP TABLE IF EXISTS `".DB_PREFIX."integrationms_product`");
    }

    public function dropIntegrationProductOptionTable()
    {
        $query = $this->db->query("
            DROP TABLE IF EXISTS `".DB_PREFIX."integrationms_product_option`");
    }

    public function alterTableOfSettings() {

    }


    public function getIntegrationProducts()
    {
        $sql = "SELECT p.product_id, pd.name, pd.language_id, ip.external_id, p.quantity, p.status FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN ".DB_PREFIX."integrationms_product ip ON  ip.product_id = p.product_id";

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $sql .= " AND p.status = 1";

        $sql .= " GROUP BY p.product_id";

        $sql .= " ORDER BY pd.name";

        $sql .= " ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        foreach ($query->rows as &$product) {
            $product['name'] = strtolower($product['name']);
        }

        return $query->rows;
    }

    public function deleteIntegrationProducts()
    {
        $sql = "DELETE FROM ".DB_PREFIX."integrationms_product";

        $query = $this->db->query($sql);

        return $query;
    }

    public function updateProductQuantity($productId, $quantity)
    {
        $sql = sprintf("UPDATE %sproduct SET quantity=%s WHERE product_id=%s",
            DB_PREFIX, $quantity, $productId);
        $query = $this->db->query($sql);

        return $query;
    }

    public function addIntegrationPrtoduct($product)
    {
        $sql = "INSERT INTO ".DB_PREFIX."integrationms_product (`product_id`, `external_id`) VALUES ('".
            $product['product_id']."', '".$product['external_id']."')";
        $query = $this->db->query($sql);

        return $query;
    }

    public function updateIntegrationProducts($integrationProductsArray)
    {
        $this->deleteIntegrationProducts();

        foreach ($integrationProductsArray as $product) {
            if($product['external_id']) {
                //var_dump($product);
                $this->addIntegrationPrtoduct($product);
                $this->updateProductQuantity($product['product_id'], $product['quantity']);
            }
        }

        return;
    }

    public function updateIntegrationOptions($integrationProductsArray)
    {
        //очистка таблицы связи товарных опций и внешних кодов
        $this->deleteIntegrationProductOptionValues();

        foreach ($integrationProductsArray as $product) {
            if($product['external_id'] && $product['modifications']) {
                //Обнуляем остатки для всех модификаций данного товара
                $this->zeroizeProductOptionValuesQuantity($product['product_id']);

                foreach($product['modifications'] as $modification) {
                    $option = $this->getOption($modification['name']);
                    //если модификации с таким имененм нет в ОС, то создадим ее
                    if (!$option) {
                        $option['option_id'] = $this->addOption($modification['name']);
                    }

                    $optionValue = $this->getOptionValue($option['option_id'], $modification['value']);
                    //echo "<br/>get_Option_value:<br/>";
                    //var_dump($optionValue);
                    if (!$optionValue) {
                        //echo "Создаем модификацию:".$modification['value'];
                        $optionValue['option_value_id'] = $this->addOptionValue($option['option_id'], $modification['value']);
                    }

                    $productOptionValue = $this->getProductOptionValue($product['product_id'], $option['option_id'], $optionValue['option_value_id']);
                    if (!$productOptionValue) {
                        //echo "Создаем ProductOptionValue:".$option['name'].":".$modification['value'];
                        $productOptionValue['product_option_value_id'] = $this->addProductOptionValue($product['product_id'], $option['option_id'], $optionValue['option_value_id'], $modification['stock']);
                    } else {
                        $this->updateProductOptionValue($productOptionValue['product_option_value_id'], $modification['stock']);
                    }

                    $this->addIntegrationProductOptionValue($productOptionValue['product_option_value_id'], $modification['externalCode']);

                }
            }
        }
    }

    public function getOption($optionName)
    {
        $sql = sprintf("Select * from %soption_description WHERE name='%s'",
            DB_PREFIX,$optionName);
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function addOption($optionName)
    {
        $sql = "INSERT INTO `".DB_PREFIX."option` (`type`, `sort_order`) VALUES ('select', 0)";
        $this->db->query($sql);
        $optionId = $this->db->getLastId();
        if(!$optionId) {
            throw new IntegrationMSModelException("Не удалось создать опцию");
        }
        $sql = sprintf("INSERT INTO `%soption_description` (`option_id`, `language_id`, `name`) VALUES ('%s', '%s', '%s')",
            DB_PREFIX, $optionId, (int)$this->config->get('config_language_id'), $optionName);
        $this->db->query($sql);

        return $optionId;
    }

    public function getOptionValue($optionId, $optionValue)
    {
        $sql = sprintf("Select * from %soption_value_description WHERE name='%s' AND option_id=%s",
            DB_PREFIX, $optionValue, $optionId);
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function addOptionValue($optionId, $optionValue)
    {
        //echo "Создаем option_value:<br/>";
        $sql = sprintf("INSERT INTO `%soption_value` (`option_id`, `image`, `sort_order`) VALUES (%s, '%s', 0)",
            DB_PREFIX, $optionId, 'no_image.jpg');
        $this->db->query($sql);
        $optionValueId = $this->db->getLastId();

        //var_dump($optionValueId);
        if(!$optionValueId) {
            throw new IntegrationMSModelException("Не удалось создать pначение опции");
        }
        //echo "Создаем option_value_description:<br/>";
        $sql = sprintf("INSERT INTO `%soption_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES ('%s', '%s', '%s', '%s')",
            DB_PREFIX, $optionValueId, (int)$this->config->get('config_language_id'), $optionId, $optionValue);
        $this->db->query($sql);
        //echo "success";
        return $optionValueId;
    }

    public function getProductOption($productId, $optionId)
    {
        $sql = sprintf("Select * from %sproduct_option WHERE product_id=%s AND option_id=%s",
            DB_PREFIX, $productId, $optionId);
        $query = $this->db->query($sql);

        return $query->row;
    }


    public function getProductOptionValue($productId, $optionId, $optionValueId)
    {
        $sql = sprintf("Select * from %sproduct_option_value WHERE product_id=%s AND option_id=%s AND option_value_id=%s",
            DB_PREFIX, $productId, $optionId, $optionValueId);
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function addProductOption($productId, $optionId)
    {
        //echo "Создаем Product option:<br/>";
        $sql = sprintf("INSERT INTO `%sproduct_option` (`product_id`, `option_id`, `required`) VALUES (%s, %s, %s)",
            DB_PREFIX, $productId, $optionId, 1);
        $this->db->query($sql);
        $productOptionId = $this->db->getLastId();

        if(!$productOptionId) {
            throw new IntegrationMSModelException("Не удалось создать product_option");
        }

        return $productOptionId;
    }

    public function addProductOptionValue($productId, $optionId, $optionValueId, $quantity)
    {
        $productOption = $this->getProductOption($productId, $optionId);

        if (!$productOption) {
            $productOption['product_option_id'] = $this->addProductOption($productId, $optionId);
        }

        //echo "Создаем product_option_value:<br/>";
        $sql = sprintf("INSERT INTO `%sproduct_option_value` (`product_option_id`, `product_id`, `option_id`, `option_value_id`, `quantity`, `subtract`) VALUES (%s, %s, %s, %s, %s, %s)",
            DB_PREFIX, $productOption['product_option_id'], $productId, $optionId, $optionValueId, $quantity, 1);
        $this->db->query($sql);
        $productOptionValueId = $this->db->getLastId();
        //echo "Product option added.<br/>";

        return $productOptionValueId;
    }

    public function zeroizeProductOptionValuesQuantity($productId)
    {
        //echo "Обнуляем остатки по опциям товара:<br/>";
        $sql = sprintf("UPDATE `%sproduct_option_value` SET `quantity`=0 WHERE `product_id`=%s",
            DB_PREFIX, $productId);

        $result = $this->db->query($sql);

        return $result;
    }

    public function updateProductOptionValue($productOptionValueId, $quantity)
    {
        //echo "Обновляем product_option_value:<br/>";
        $sql = sprintf("UPDATE `%sproduct_option_value` SET `quantity`=%s WHERE `product_option_value_id`=%s",
            DB_PREFIX, $quantity, $productOptionValueId);

        $result = $this->db->query($sql);

        //echo "Product_option_Value updated:".$productOptionValueId.":".$quantity."<br/>";

        return $result;
    }


    public function deleteIntegrationProductOptionValues()
    {
        $sql = sprintf("DELETE FROM `%sintegrationms_product_option`", DB_PREFIX);
        $result = $this->db->query($sql);

        return $result;
    }

    public function addIntegrationProductOptionValue($productOptionValueId, $externalCode)
    {
        $sql = sprintf("INSERT INTO `%sintegrationms_product_option` (`product_option_value_id`, `external_id`) VALUES (%s, '%s')",
            DB_PREFIX, $productOptionValueId, $externalCode);
        $result = $this->db->query($sql);

        $integrationProductOptionId = $this->db->getLastId();

        return $integrationProductOptionId;
    }
    
    public function getIntegrationProductOptionValue($productOptionValueId)
    {
        $sql = sprintf("Select * from %sintegrationms_product_option WHERE product_option_value_id=%s",
            DB_PREFIX, $productOptionValueId);
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getIntegrationProduct($productId)
    {
        $sql = sprintf("Select * from %sintegrationms_product WHERE product_id=%s",
            DB_PREFIX, $productId);
        $query = $this->db->query($sql);

        return $query->row;
    }


}