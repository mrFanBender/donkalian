<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/');

// HTTPS
define('HTTPS_SERVER', 'https://localhost/');

// DIR
define('DIR_APPLICATION', '/var/www/project/catalog/');
define('DIR_SYSTEM', '/var/www/project/system/');
define('DIR_DATABASE', '/var/www/project/system/database/');
define('DIR_LANGUAGE', '/var/www/project/catalog/language/');
define('DIR_TEMPLATE', '/var/www/project/catalog/view/theme/');
define('DIR_CONFIG', '/var/www/project/system/config/');
define('DIR_IMAGE', '/var/www/project/image/');
define('DIR_CACHE', '/var/www/project/system/cache/');
define('DIR_DOWNLOAD', '/var/www/project/download/');
define('DIR_LOGS', '/var/www/project/system/logs/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'mysql');
define('DB_USERNAME', 'u0099795_default');
//define('DB_USERNAME', 'admin');
define('DB_PASSWORD', 'u0A5C_FO');
//define('DB_PASSWORD', 'admin');
define('DB_DATABASE', 'u0099795_default');
define('DB_PREFIX', 'oc_');
?>