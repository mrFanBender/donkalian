<?php
     $_['heading_title'] = 'Модуль интеграции с МойСклад';
    // Text
    $_['text_module']             = 'Модули';
    $_['text_success']            = 'Настройки успешно обновлены!';

    // Error
    $_['error_permission']        = 'У Вас нет прав для изменения модуля "Интеграция с МойСклад"!';
