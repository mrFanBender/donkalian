$(document).ready(function() {
	product_object ={
		id: '',
		name:'',
		price: '',
		brand:'',
		category:'',
		quantity:''
	};
	product_object.id = $('input[name="product_id"]').val();
	product_object.name = $('h1[name="product_name"]').html();
	if($('#product_price').html()){
		product_object.price = $('#product_price').html();
	}else if($('#product_price_new').html()){
		product_object.price = $('#product_price_new').html();
	}
	product_object.price = parseFloat(product_object.price);
	product_object.brand = $('#brand_name').html();
	$('#breadcrumb li:gt(0) > a > span').each(function(index, element){
		product_object.category += $(element).html()+'/';
	});
	ecommerce.clear_fields();
	ecommerce.add_product(product_object.id, product_object.name, product_object.price, 1, product_object.brand, product_object.category);
	productDetail(ecommerce.products);

	
	$('#button-cart').click(function(){
		product_object.quantity = $('input[name="quantity"]').val();
		ecommerce.clear_fields();
		ecommerce.add_product(product_object.id, product_object.name, product_object.price, product_object.quantity, product_object.brand, product_object.category);
		productAddToCart(ecommerce.products);
	});
	/*$('#buy_1_click_button').click(function(){
		product_object.quantity = 1;
		productAddToCart(product_object);
	});*/

});