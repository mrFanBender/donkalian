<?php
// Heading
$_['heading_title']       = 'Пакетная загрузка товаров';

// Text
$_['text_module']         = 'Модули дфдф';
$_['text_success']        = 'Настройки модуля обновлены!';
$_['text_content_top']    = 'Верх страницы';
$_['text_content_bottom'] = 'Низ страницы';
$_['text_column_left']    = 'Левая колонка';
$_['text_column_right']   = 'Правая колонка';

// Entry
$_['entry_layout']        = 'Схема:';
$_['entry_position']      = 'Позиция:';
$_['entry_status']        = 'Статус:';
$_['entry_sort_order']    = 'Порядок сортировки:';

// Error
$_['error_permission']    = 'У Вас нет прав для управления этим модулем!';
$_['warning_select_image']    = 'Выберите изображение!';
?>